### EgoServ-LDAP

This is in a very early development stage, and so is the upstream EgoServ plugin.

Goals:
	- Extend EgoServ to sync user details and modifications with an external backend (LDAP)
	- Extend EgoServ to modify user attributes beyond IRC
	- Make EgoServ attractive to federated environments
	- Allow for easier permission management through LDAP group assignments

----
### Original README.md:
Suite of tools to help Network Operators run their ErgoIRCd nets
